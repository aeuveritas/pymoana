import Moana.Library.MainClass.MainClass as mMainClass
import Moana.Core.Core as mCore
import Boat.Client.Client as mClient

class Boat(mMainClass.MainClass):
    def __init__(self, params):
        super(Boat, self).__init__(params)
        self._core = mCore.Core()
        self._client = mClient.Client()

        self._createWorkers()

    def _createWorkers(self):
        self._core.createWorkers()
        self._client.createWorkers()

    def run(self):
        self._core.runWorkers()
        self._core.printDebugInfo()

        ret = self._core.controlMainQueue()

        if ret in (0, 1):
            self._terminator.stop()
        else:
            self._terminator.terminate()
