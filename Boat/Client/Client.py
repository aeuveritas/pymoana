import Moana.Library.MainClass.MainClass as mMainClass

import Boat.Client.Interface.Interface as mInterface
import Boat.Client.Commander.Commander as mCommander

class Client(mMainClass.MainClass):
    def __init__(self):
        super(Client, self).__init__()

        self._interface = mInterface.Interface()
        self._commander = mCommander.Commander()

    def createWorkers(self):
        self._interface.createWorker()
        self._commander.createWorkers()
