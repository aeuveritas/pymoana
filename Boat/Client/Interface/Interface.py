import sys
from multiprocessing import Process

import Boat.Client.Interface.Console.Console as mConsole
import Moana.Library.MainClass.MainClass as mMainClass


class Interface(mMainClass.MainClass):
    def __init__(self):
        super(Interface, self).__init__()

        self._interfaceQueue = self._queueManager.createQueue("interface")

        self._interface = None
        self._worker = None

        self._interfaceType = self._ruler.getConfig("interface")

    def createWorker(self):
        self._logger.debug("Create Interface Worker")

        if self._interfaceType == "console":
            self.createConsole()
        else:
            self._logger.critical("Wrong interface type")
            self._terminator.stop()

        self._registerRole()

    def _registerRole(self):
        self._workerManager.setRole(
            "{0}".format(self), "Core")

    def createConsole(self):
        self._interface = mConsole.Console()
        target = self._interface.runInput
        fileno = sys.stdin.fileno()

        self._worker = \
            Process(target = target, \
                    args=("Console", fileno,))
        self._workerManager.addWorker("Console", \
                                   self._worker, \
                                   self)

    def executeFrame(self, frame):
        self._logger.critical("TBD")