import os
import sys

import Moana.Library.ProcessClass.ProcessClass as mProcessClass

class Console(mProcessClass.ProcessClass):
    def __init__(self):
        super(Console, self).__init__()
        self._name = self._ruler.getConfig("machine")

        self._menu = []
        self._menu.append("Exit")

        self._menu.append("Ping")

    def _printMenu(self):
        menu = 0

        while True:
            print("PyBoat (PyMoana client) - v.{0}".format( \
                self._ruler.getConfig("version")))

            for i in range(1, len(self._menu)):
                print("{0}. {1}".format(i, self._menu[i]))
            print("{0}. {1}".format(0, self._menu[0]))

            try:
                menu = int(input("Menu: "))
                if menu in range(0, len(self._menu)):
                    break
                else:
                    print( \
                        "Please select menu (0 - {0})".format(
                            len(self._menu) - 1))
            except:
                continue

        return self._menu[menu]

    def _printExit(self):
        print("Thank you for using PyBoat.")

    def runInput(self, workerName, fileno):
        self._workerManager.setCurrentWorker(workerName)

        sys.stdin = os.fdopen(fileno)

        while True:
            menu = self._printMenu()

            src = self._json.getPacketSrc(self)

            machine = "Server"
            exe = "Moana"
            role = "Commander"

            dest = self._json.getDest(
                machine = machine, \
                exe = exe, \
                role = role)

            frame = self._json.getFrame(\
                src=src, \
                dest=dest, \
                cmd=menu)

            core = self._workerManager.getHandler(role)
            core.putQueue(frame, self)

            if menu == "Exit":
                self._printExit()
                break



