import Moana.Library.ProcessClass.ProcessClass as mProcessClass

class CmdPing(mProcessClass.ProcessClass):
    def __init__(self):
        super(CmdPing, self).__init__()

    def execute(self, managerName):
        self._workerManager.setCurrentWorker(managerName)

        topic = self._ruler.getConfig("master")

        src = self._json.getPacketSrc(self)

        machine = "Server"
        exe = "Moana"
        role = "Executor"

        dest = self._json.getDest(\
            machine=machine,\
            exe=exe, \
            role=role)

        packet = self._json.getPacket( \
            topic=topic, \
            src=src, \
            dest=dest, \
            cmd="PingRequester")

        messenger = self._workerManager.getHandler("Publisher")
        messenger.sendPacket(packet)

        #interfaceQueue = self._queueManager.loadQueue("Interface")
        #frame = interfaceQueue.get()

        #self._logger.debug("Frame: {0}".format(frame))