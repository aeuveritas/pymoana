import Moana.Library.ProcessClass.ProcessClass as mProcessClass

class CmdExit(mProcessClass.ProcessClass):
    def __init__(self):
        super(CmdExit, self).__init__()

    def execute(self, managerName):
        src = self._json.getPacketSrc(self)

        dest = self._json.getDest( \
            role = "Core")

        frame = self._json.getFrame(\
            src = src,\
            dest = dest,
            cmd="Exit")

        main = self._workerManager.getHandler("Core")
        main.putQueue(frame, self)