import Boat.Client.Commander.Order.CmdExit as mExit

import Boat.Client.Commander.Order.CmdPing as mCmdPing
import Moana.Library.MainClass.MainClass as mMainClass

class Commander(mMainClass.MainClass):
    def __init__(self):
        super(Commander, self).__init__()

        self._managers = {"Ping":mCmdPing.CmdPing(), \
                          "Exit":mExit.CmdExit()}

    def createWorkers(self):
        self._registerRole()
        self._registerManagers()

    def _registerManagers(self):
        for k, v in self._managers.items():
            target = v.execute
            self._executor.registerExecutor(target, k, self)

    def _registerRole(self):
        self._workerManager.setRole(
            "{0}".format(self), "Core")

    def executeFrame(self, frame):
        cmd = frame["cmd"]

        self._executor.startWorker(cmd)
