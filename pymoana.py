#!/usr/bin/env python3
import Moana.Moana as mMoana
import ArgParser as argParser

def main():
    parser = argParser.ArgParser()
    params = parser.parseMoana()

    moana = mMoana.Moana(params)

    moana.run()

if __name__ == "__main__":
    main()