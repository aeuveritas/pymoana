import zmq

import Moana.Core.Messenger.PubSub.PubSubZmq as mPubSubZmq
import Moana.Library.ProcessClass.ProcessClass as mProcessClass

class Subscriber(mProcessClass.ProcessClass):
    def __init__(self):
        super(Subscriber, self).__init__()

    def initZmq(self):
        self._zmq = mPubSubZmq.PubSubZmq(self)
        self._class = self._ruler.getConfig("class")

        self._ip = None
        self._port = None
        self._type = "{0}".format(self)

        if self._class == "master":
            self._ip = "*"
            self._port = self._ruler.getConfig("master port")
        elif self._class == "servant":
            # self.__master = self._ruler.getConfig("master")
            self._ip = "localhost"
            self._port = self._ruler.getConfig("servant port")
        else:
            self._logger.critical("Wrong class type")

        self._zmq.setUrl(self._ip, self._port, self)
        self._zmq.initZmq(self._type)

    def run(self, workerName):
        self._workerManager.setCurrentWorker(workerName)
        self.initZmq()
        
        while True:
            frame = self._zmq.recvPacket()

            dest = frame["dest"]["role"]

            if dest in self._workerManager.getHandlersList():
                worker = self._workerManager.getHandler(dest)
                worker.putQueue(frame, self)
            else:
                main = self._workerManager.getHandler("Core")
                main.putQueue(frame, self)
