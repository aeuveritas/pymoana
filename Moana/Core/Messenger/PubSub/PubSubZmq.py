import zmq

import Moana.Library.BaseClass.BassClass as mBaseClass

class PubSubZmq(mBaseClass.BaseClass):
    def __init__(self, host):
        super(PubSubZmq, self).__init__()
        self._machine = self._ruler.getConfig("machine")
        self._host = host
        self._version = self._ruler.getConfig("version")

        self._class = self._ruler.getConfig("class")

        self._logger.debug("ZMQ for {0}".format(self._host))

    def initZmq(self, type):
        if type == "Publisher":
            self._type = zmq.PUB
        elif type == "Subscriber":
            self._type = zmq.SUB
        else:
            self._logger.critical("Wrong zmq type: {0}".format(type))
            self._terminator.terminate()

        self._context = zmq.Context()
        self._socket = self._context.socket(self._type)

        if self._class == "master":
            self._socket.bind(self._url)
        elif self._class == "servant":
            self._socket.connect(self._url)
        else:
            self._logger.critical("Wrong class type")

        if self._type == zmq.SUB:
            self._socket.setsockopt_string(zmq.SUBSCRIBE, \
                                           "ALL")
            self._socket.setsockopt_string(zmq.SUBSCRIBE, \
                                           self._machine)

    def setUrl(self, ip, port, mode):
        self._ip = ip
        self._port = port
        self._mode = mode

        self._url = "tcp://" + self._ip +  ":" + self._port

        self._logger.debug("{0} URL: {1}".format(self._mode, \
                                                 self._url))

    def sendPacket(self, packet):
        self._socket.send_string(packet)
        self.sendLog(packet)

    def recvPacket(self):
        self._logger.debug("Wait({0}:{1})".format(self._machine, \
                                                  self._port))
        packet = self._socket.recv_string()
        self.recvLog(packet)

        version, frame = self._json.packetToFrame(packet)

        if version != self._version:
            self._logger.waring( \
                "Different version. send: {0}, recv: {0}".format( \
                    frame["version"], self._version))

        return frame

    def sendLog(self, packet):
        self._logger.debug("({0} - {1}) {2}". \
                           format(self._machine, \
                                  self._host, \
                                  packet))

    def recvLog(self, packet):
        self._logger.debug("({0} - {1}) {2}". \
                           format(self._machine, \
                                  self._host, \
                                  packet))
