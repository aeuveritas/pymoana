import zmq

import Moana.Core.Messenger.PubSub.PubSubZmq as mPubSubZmq
import Moana.Library.ProcessClass.ProcessClass as mProcessClass

class Publisher(mProcessClass.ProcessClass):
    def __init__(self):
        super(Publisher, self).__init__()
        self._getQueue = self._queueManager.loadQueue("Publisher")

    def initZmq(self):
        self._zmq = mPubSubZmq.PubSubZmq(self)
        self._class = self._ruler.getConfig("class")

        self._ip = None
        self._port = None
        self._type = "{0}".format(self)

        if self._class == "master":
            self._ip = "*"
            self._port = self._ruler.getConfig("servant port")
        elif self._class == "servant":
            #self.__master = self._ruler.getConfig("master")
            self._ip = "localhost"
            self._port = self._ruler.getConfig("master port")
        else:
            self._logger.critical("Wrong class type")

        self._zmq.setUrl(self._ip, self._port, self)
        self._zmq.initZmq(self._type)

    def run(self, workerName):
        self._workerManager.setCurrentWorker(workerName)
        self.initZmq()

        while True:
            packet = self._getQueue.get()
            self._json.movingPacket("Messenger", self, packet)

            self._zmq.sendPacket(packet)

    def sendPacket(self, packet):
        self._zmq.sendPacket(packet)