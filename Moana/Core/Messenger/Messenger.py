from multiprocessing import Process

import Moana.Core.Messenger.PubSub.Publisher as mPublisher

import Moana.Core.Logger.Logger as mLogger
import Moana.Core.Messenger.PubSub.Subscriber as mSubscriber
import Moana.Core.QueueManager.QueueManager as mQueueManager
import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.Json.Json as mJson
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager

class BaseMessenger:
    def __init__(self):
        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()
        self._json = mJson.Json.json()

        self._workerManager = mWorkerManager.WorkerManager.workerManager()
        self._queueManager = mQueueManager.QueueManager.queueManager()

        self._class = self._ruler.getConfig("class")

        self._pubQueue = self._queueManager.createQueue("Publisher")
        self._reqQueue = self._queueManager.createQueue("Request")
        self._repQueue = self._queueManager.createQueue("Reply")

        self._messenger = {}

        self._logger.info("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def _createPubSub(self):
        self._logger.debug("Create Pub Sub pattern")

        name = "Subscriber"
        subscriber = mSubscriber.Subscriber()
        self._messenger[name] = \
            Process(target=subscriber.run, \
                    args=(name,))
        self._workerManager.addWorker( \
            name, self._messenger[name], self)

        publisher = mPublisher.Publisher()
        name = "Publisher"
        self._messenger[name] = \
            Process(target=publisher.run, \
                    args=(name,))
        self._workerManager.addWorker( \
            name, self._messenger[name], self)

    def _createReqRep(self):
        self._logger.debug("Create Req Rep pattern")

    def createWorkers(self):
        classType = self._ruler.getConfig("class")

        if classType in ("master", "servant"):
            self._createPubSub()

        if classType in ("servant", "client"):
            self._createReqRep()

        self._createReqRep()

    def sendPacket(self, packet, caller = None):
        self._json.movingPacket(caller, self, packet)

        if self._pubQueue:
            self._pubQueue.put(packet)
        else:
            self._logger.critical("pubQueue is not set")

class Messenger(BaseMessenger):
    _instance = None

    @classmethod
    def messenger(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return cls._instance