import Moana.Library.MainClass.MainClass as mMainClass


class Core(mMainClass.MainClass):
    """Core class"""

    def __init__(self, params = None):
        """
        Core class constructor
        :param params: I. parameters for config
        :return:
        """
        super(Core, self).__init__(params)

        self._coreQueue = self._queueManager.createQueue("Core")

    def createWorkers(self):
        """
        Create workers
            - Messenger
            - DataManager
            - register roles
        :return:
        """
        self._workerManager.addWorker("{0}".format(self), self, self)

        self._messenger.createWorkers()
        self._dataManager.createWorkers()

        self._registerRoles()

    def _registerRoles(self):
        """
        Register roles for sub class
            - Executor
        :return:
        """
        self._executor.registerRole()

    def putQueue(self, frame, caller):
        """
        Put frame to main queue
        :param frame: I. frame
        :param caller: I. caller
        :return:
        """
        self._json.movingFrame(caller, self, frame)
        self._coreQueue.put(frame)

    def getQueue(self):
        """
        Get frame from main queue
        :return:
        """
        return self._coreQueue.get()

    def controlMainQueue(self):
        """
        Get frame from main queue and pass the frame to target
        :return:
        """
        ret = "normal"

        while True:
            try:
                frame = self.getQueue()

                role = frame["dest"]["role"]

                handler = self._workerManager.getHandler(role)
                if handler == self:
                    output = handler.executeFrame(frame)
                    if output == "Exit":
                        break
                else:
                    handler.putQueue(frame, self)

            except KeyboardInterrupt:
                ret = "keyboard"
                break

        return ret

    def executeFrame(self, frame):
        """
        Execute frame
        :param frame: I. frame
        :return:
        """
        ret = None
        src = frame["src"]
        cmd = frame["cmd"]

        if cmd == "Exit":
            ret = "Exit"
        elif cmd == "finish":
            self._executor.deleteWorker(src)

        return ret

    def printDebugInfo(self):
        """
        Print workers, handlers, roles, queues
            - if action is "debug"
        """
        action = self._ruler.getConfig("action")

        if action == "debug":
            workers = self._workerManager.getWorkersList()
            self._logger.debug("Workers: {0}".format(workers))
            handlers = self._workerManager.getHandlersList()
            self._logger.debug("Handlers: {0}".format(handlers))
            roles = self._workerManager.getRoles()
            self._logger.debug("Roles: {0}".format(roles))
            queues = self._queueManager.getQueuesList()
            self._logger.debug("Queues: {0}".format(queues))

    def runWorkers(self):
        """
        Run workers
        :return:
        """
        self._workerManager.runWorkers()

    def terminate(self):
        """
        Terminate
        :return:
        """
        self._terminator.terminate()

    def stop(self):
        """
        Stop
        :return:
        """
        self._terminator.stop()

    def doInitialPing(self):
        """
        Try initial ping
        :return:
        """
        if self._ruler.getConfig("class") == "master":
            self._executor.startWorker("PingRequester")