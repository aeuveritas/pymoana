import sys
import time

import Moana.Core.Logger.Logger as mLogger
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager

class BaseTerminator:
    def __init__(self):
        self._logger = mLogger.Logger.logger()
        self._workerManager = mWorkerManager.WorkerManager.workerManager()

        self._logger.debug("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def stop(self):
        self._logger.info("Stop...")
        self._workerManager.stop()
        time.sleep(3)

    def terminate(self):
        self._logger.info("Terminate...")
        self._workerManager.stop()
        sys.exit(2)

class Terminator(BaseTerminator):
    _instance = None

    @classmethod
    def terminator(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return cls._instance