import json
import collections

import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.Logger.Logger as mLogger
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager

class BaseJson:
    def __init__(self):
        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()
        self._workerManager = mWorkerManager.WorkerManager.workerManager()

        self._machine = self._ruler.getConfig("machine")
        self._exe = self._ruler.getConfig("exe")

        self._logger.debug("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def getPacketSrc(self, src = None):
        worker = self._workerManager.getCurrentWorker()

        frameSrc = collections.OrderedDict()
        frameSrc["machine"] = self._machine
        frameSrc["exe"] = self._exe
        frameSrc["worker"] = worker
        frameSrc["role"] = "{0}".format(src)

        return frameSrc

    def getDest(self, machine = None, exe = None, worker = None, role = None):
        frameDest = collections.OrderedDict()
        frameDest["machine"] = machine
        frameDest["exe"] = exe
        frameDest["worker"] = worker
        frameDest["role"] = role

        return frameDest

    def getFrame(self, src = None, dest = None, cmd = None, \
                 args = None, msg = None):
        frame = {"src"  : src, \
                 "dest" : dest, \
                 "cmd"  : cmd, \
                 "args" : args, \
                 "msg"  : msg}

        return frame

    def _getZFrame(self, frame):
        frame["version"] = self._ruler.getConfig("version")

    def _getQFrame(self, frame):
        version = frame["version"]

        del frame["version"]

        return version

    def getPacket(self, topic = None, src = None, dest = None, \
                  cmd = None, args = None, msg = None):
        frame = self.getFrame(src, dest, cmd, args, msg)
        self._getZFrame(frame)
        packet = self._frameToPacket(topic, frame)
        return packet

    def _frameToPacket(self, topic, frame):
        jsonData = self._frameToJson(frame)
        packet = topic + ' ' + jsonData
        return packet

    def packetToFrame(self, packet):
        token = packet.find('{')
        topic = packet[0:token - 1].strip()
        string = packet[token:]
        frame = self._stringToFrame(string)
        version = self._getQFrame(frame)
        return version, frame

    def _frameToJson(self, frame):
        jsonData = json.dumps(frame)
        return jsonData

    def _stringToFrame(self, string):
        frame = json.loads(string)
        return frame

    def movingFrame(self, src, dest, frame):
        self._logger.debug(
            "[F] {0} >> {1}: {2}".format(src, dest, frame))

    def movingPacket(self, src, dest, packet):
        self._logger.debug(
            "[P] {0} >> {1}: {2}".format(src, dest, packet))

class Json(BaseJson):
    _instance = None

    @classmethod
    def json(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance
