import os
import json

class BaseRuler:
    class _Configs:
        _configs = {}

        def __init__(self, params):
            self._configs = params

        def __repr__(self):
            msg = ""
            for k, v in self._configs.items():
                msg += """
    {0} : {1}""".format(k, v)

            return msg

        def getConfig(self, config):
            return self._configs[config]

        def setConfig(self, config, value):
            self._configs[config] = value

    def __init__(self, params = None):
        self._configs = self.setConfigs(params["configFile"])
        self._configs.setConfig("exe", params["exe"])
        self._configs.setConfig("action", params["action"])

        if params["action"] == "debug":
            if "class" in params:
                classType = params["class"]
                if classType:
                    self._configs.setConfig("class", classType)

            if "machine" in params:
                machine = params["machine"]
                if machine:
                    self._configs.setConfig("machine", machine)

            print("[DBG] Configs {0}".format(self._configs))

    def __repr__(self):
        return type(self).__name__

    def setConfigs(self, configFile = None):
        if configFile:
            configFile = os.path.abspath(configFile)
            if not os.path.isfile(configFile):
                print("Log File({0}) is not exist".format(configFile))

        with open(configFile) as config:
            params = json.load(config)

            configs = self._Configs(params)

        return configs

    def getConfig(self, config):
        return self._configs.getConfig(config)

    def getConfigs(self):
        return self._configs

class Ruler(BaseRuler):
    _instance = None

    @classmethod
    def ruler(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance