import time

import Moana.Core.Logger.Logger as mLogger
import Moana.Core.Ruler.Ruler as mRuler

class BaseWorkerManager:
    def __init__(self):
        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()

        self._workers = {} # Processes
        self._instanceWorkers = {} # Processes
        self._handlers = {} # interface class
        self._roles = {} # name

        self._rWorkers = {} # Running workers
        self._currentWorker = "Core"

        self._logger.debug("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def _addHandler(self, name, handler):
        self._handlers[name] = handler
        self._roles[name] = [name]

    def addWorker(self, name, worker, handler):
        workerType = worker.__class__.__name__
        self._logger.debug(
            "Add {0} worker: {1}".format(workerType, name))

        if name == "Core":
            self._addHandler(name, handler)
        if workerType == "Process":
            self._workers[name] = worker
            self._addHandler(name, handler)
        else:
            self._logger.critical(
                "Wrong worker type: {0}".format(workerType))

    def setRole(self, role, worker):
        self._roles[worker].append(role)
        self._logger.debug(
            "Append role {0} to {1}".format(role, worker))

    def getRoles(self):
        return self._roles

    def getHandler(self, role):
        handler = None

        for k, v in self._roles.items():
            if role in v:
                handler = k
                break

        if not handler:
            self._logger.critical(
                "Wrong role: {0}".format(role))

        return self._handlers[handler]

    def _runWorker(self, worker, name):
        if name in self._rWorkers:
            self._logger.debug("Already running: {0}".format(name))
            return

        worker.start()
        self._rWorkers[name] = worker
        self._logger.debug("Run: {0}".format(name))

    def runMessenger(self):
        messengers = None

        exe = self._ruler.getConfig("exe")
        if  exe == "Moana":
            messengers = ("Publisher", "Subscriber")
        elif exe == "Boat":
            messengers = ("Requester", "Replyer")
        else:
            self._logger.critical(
                "Wrong exe type: {0}".format(exe))

        for messenger in messengers:
            if messenger in self._workers:
                worker = self._workers[messenger]
                self._runWorker(worker, messenger)

        time.sleep(1)

    def runWorkers(self):
        self.runMessenger()

        for name in self._workers.keys():
            worker = self._workers[name]
            self._runWorker(worker, name)

    def runInstanceWorker(self, name, worker, handler):
        workerType = worker.__class__.__name__
        self._logger.debug(
            "Add {0} instance worker: {1}".format(workerType, name))

        if workerType == "Process":
            self._instanceWorkers[name] = worker
            self._addHandler(name, handler)
        else:
            self._logger.critical(
                "Wrong worker type: {0}".format(workerType))

        self._runWorker(worker, name)
    def stop(self):
        for workerName, worker in self._rWorkers.items():
            worker.terminate()
            self._logger.debug("Terminate: {0}".format(workerName))

    def getWorkersList(self):
        return self._workers

    def deleteRunningWorker(self, name):
        del self._rWorkers[name]

        self._logger.debug(
            "Worker deleted: {0}".format(name))

    def getHandlersList(self):
        return self._handlers

    def getWorkerType(self):
        types = []
        for k, v in self._workers.items():
            types.append(type(v))

        return types

    def getHandlerType(self):
        types = []
        for k, v in self._handlers.items():
            types.append(type(v))

        return types

    def getCurrentWorker(self):
        return self._currentWorker

    def setCurrentWorker(self, worker):
        self._currentWorker = worker

    def getRunningWorkersList(self):
        return self._rWorkers

class WorkerManager(BaseWorkerManager):
    _instance = None

    @classmethod
    def workerManager(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance