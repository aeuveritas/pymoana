from subprocess import Popen
from subprocess import check_output

import Moana.Library.BaseClass.BassClass as mBaseClass

class BaseUpdater(mBaseClass.BaseClass):
    def __init__(self):
        super(BaseUpdater, self).__init__()
        self._repo = self._ruler.getConfig("git repo")

    def __repr__(self):
        return type(self).__name__

    def update(self):
        self._logger.info("Update...")

        fetch = Popen("git fetch", shell=True)
        self._logger.info("{0}".format(fetch.args))

        out = check_output("git log -1", shell=True)
        self._logger.info("From:\n{0}".format(out.decode()))

        out = check_output("git rebase", shell=True)
        self._logger.info("\n{0}".format(out.decode()))

        out = check_output("git log -1", shell=True)
        self._logger.info("To:\n{0}".format(out.decode()))

class Updater(BaseUpdater):
    _instance = None

    @classmethod
    def updater(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance