import logging
import logging.handlers
import os
import sys
import datetime

import Moana.Core.Ruler.Ruler as mRuler

class BaseLogger:
    _logger = None

    class _Logger:
        def __init__(self, exe = None, logType = "all", classType = None):
            # Interface logging
            self._kLogging = logging.getLogger(exe)
            self._kLogging.setLevel(logging.DEBUG)

            if exe == "Moana":
                if logType == "print":
                    self.createPrintHandler()
                elif logType == "write":
                    self.createWriteHandler(classType, exe)
                elif logType == "all":
                    self.createPrintHandler()
                    self.createWriteHandler(classType, exe)
                else:
                    self._kLogging.critical( \
                        "{0} is not correct type for logger".format(logType))
                    sys.exit(2)
            elif exe == "Boat":
                self.createWriteHandler(classType, exe)

        def createPrintHandler(self):
            streamHandler = logging.StreamHandler()
            streamLogFormat = \
                '[ %(process)s | %(levelname)s - %(filename)s : %(lineno)s ] %(message)s'
            streamFormatter = logging.Formatter(streamLogFormat)
            streamHandler.setFormatter(streamFormatter)
            self._kLogging.addHandler(streamHandler)

        def createWriteHandler(self, classType, exe):
            dirName = "./log"

            if not os.path.isdir(dirName):
                os.mkdir(dirName)

            now = datetime.datetime.now()
            logFileName = dirName + "/" + exe + now.strftime("-%Y-%m-%d") \
                          + "-" + classType + ".log"
            maxFileSize = 100 * 1024 * 1024
            fileHandler = logging.handlers.RotatingFileHandler( \
                filename=logFileName, maxBytes=maxFileSize)
            fileLogFormat = \
                '[ %(process)s |  %(levelname)s - %(filename)s : %(lineno)s ] ( %(asctime)s ) %(message)s'
            fileFormatter = logging.Formatter(fileLogFormat)
            fileHandler.setFormatter(fileFormatter)
            self._kLogging.addHandler(fileHandler)

        @property
        def getLogger(self):
            return self._kLogging

        def setLogLevel(self, logLevel):
            if logLevel == "debug":
                self._kLogging.setLevel(logging.DEBUG)
            elif logLevel == "info":
                self._kLogging.setLevel(logging.INFO)
            elif logLevel == "warning":
                self._kLogging.setLevel(logging.WARNING)
            elif logLevel == "error":
                self._kLogging.setLevel(logging.ERROR)
            elif logLevel == "critical":
                self._kLogging.setLevel(logging.CRITICAL)
            else:
                self._kLogging.critical( \
                    "{0} is not correct type for log".format(logLevel))
                self._kLogging.setLevel(logging.CRITICAL)

    def __init__(self):
        self._ruler = mRuler.Ruler.ruler()
        exe = self._ruler.getConfig("exe")
        logType = self._ruler.getConfig("log type")
        classType = self._ruler.getConfig("class")
        self._logger = self._Logger(exe, logType, classType)

    def __repr__(self):
        return type(self).__name__

    @property
    def getLogger(self):
        return self._logger.getLogger

    def setLogLevel(self, logLevel):
        self._logger.setLogLevel(logLevel)

    def setLogger(self, logType = None, classType = None):
        del self._logger
        exe = self._ruler.getConfig("exe")
        self._logger = self._Logger(exe, logType, classType)

class Logger(BaseLogger):
    _instance = None

    @classmethod
    def logger(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance.getLogger

    @classmethod
    def loggerManager(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance
