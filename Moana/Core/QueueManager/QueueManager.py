from multiprocessing import Queue

import Moana.Core.Logger.Logger as mLogger
import Moana.Core.Json.Json as mJson
import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager

class BaseQueueManager:
    def __init__(self):
        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()

        self._queues = {}

        self._logger.info("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def createQueue(self, name):
        queue = None

        if name in self._queues:
            queue = self._queues.get(name)
            self._logger.debug("Existed queue: {0}".format(name))
        else:
            queue = Queue()
            self._queues[name] = queue
            self._logger.debug("Create queue: {0}".format(name))

        return queue

    def loadQueue(self, name):
        queue = None
        if name in self._queues:
            queue = self._queues.get(name)
        else:
            self._logger.critical("Undefined queue: {0}".format(name))

        return queue

    def getQueuesList(self):
        keys = list(self._queues.keys())
        keys.sort()

        return keys

class QueueManager(BaseQueueManager):
    _instance = None

    @classmethod
    def queueManager(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return  cls._instance