from multiprocessing import Process

import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager
import Moana.Core.Logger.Logger as mLogger
import Moana.Core.Json.Json as mJson
import Moana.Core.QueueManager.QueueManager as mQueueManager

import Moana.Core.DataManager.Database as mDatabase

class BaseDataManager:
    def __init__(self):
        super(BaseDataManager, self).__init__()
        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()
        self._json = mJson.Json.json()

        self._workerManager = mWorkerManager.WorkerManager.workerManager()
        self._queueManager = mQueueManager.QueueManager.queueManager()

        self._database = None
        self._worker = None

        self._requestQueue = self._queueManager.createQueue("Data_in")
        self._replyQueue = self._queueManager.createQueue("Data_out")

        self._logger.info("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def createWorkers(self):
        self._logger.debug("Create Data Manager worker")

        self._database = mDatabase.Database()

        self._worker = Process(target=self._database.run,\
                               args=("DataManager",))
        self._workerManager.addWorker("DataManager", \
                                   self._worker, \
                                   self)

    def request(self, frame, caller):
        self._json.movingFrame(caller, self, frame)
        self._requestQueue.put(frame)

        frame = self._replyQueue.get()
        self._json.movingFrame("Database", self, frame)

        return frame

class DataManager(BaseDataManager):
    _instance = None

    @classmethod
    def dataManager(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return cls._instance