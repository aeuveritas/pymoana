import Moana.Library.ProcessClass.ProcessClass as mProcessClass

"""
Process Name: Database
Input Queue: data_in
Output Queue: data_out
Manager: DataManager
Input Method: request
"""
class Database(mProcessClass.ProcessClass):
    def __init__(self):
        super(Database, self).__init__()
        self._data = {}

        self._getQueue = self._queueManager.createQueue("Data_in")
        self._putQueue = self._queueManager.createQueue("Data_out")

    def initDatabase(self):
        pass

    def run(self, workerName):
        self._workerManager.setCurrentWorker(workerName)
        self.initDatabase()

        while True:
            frame = self._getQueue.get()

            self._logger.debug(
                "[F] {0} >> {1}: {2}".format("DataManager", self, frame))

            src = frame["src"]
            cmd = frame["cmd"]
            query = frame["msg"]

            if cmd == "write":
                self._data[src] = query
                frame["msg"] = "updated"
            elif cmd == "read":
                frame["msg"] = self._data[src]
            else:
                frame["msg"] = "fail"
                self._logger.warning("No db argument")

            frame["src"] = "{0}".format(self)

            self._putQueue.put(frame)
