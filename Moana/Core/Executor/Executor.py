from multiprocessing import Process

import Moana.Core.Logger.Logger as mLogger
import Moana.Core.Json.Json as mJson
import Moana.Core.QueueManager.QueueManager as mQueueManager
import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager

class BaseExecutor:
    def __init__(self):
        super(BaseExecutor, self).__init__()

        self._logger = mLogger.Logger.logger()
        self._ruler = mRuler.Ruler.ruler()
        self._workerManager = mWorkerManager.WorkerManager.workerManager()
        self._queueManager = mQueueManager.QueueManager.queueManager()
        self._json = mJson.Json.json()

        self._candidates = {}

        self._logger.info("Generated {0}".format(self))

    def __repr__(self):
        return type(self).__name__

    def registerRole(self):
        self._workerManager.setRole(
            "{0}".format(self), "Core")

    def startWorker(self, name):
        candidate = self._candidates[name]
        handler = candidate["handler"]
        function = candidate["function"]

        worker = Process(target = function,
                         args=(name, ))
        self._workerManager.runInstanceWorker(name, worker, handler)

        self._logger.debug("Process done: {0}".format(name))

    def registerExecutor(self, runFunc, name, handler):
        self._candidates[name] = { "handler": handler,
                                   "function" : runFunc}
        self._logger.debug("Register Executor: {0}".format(name))

    def deleteWorker(self, name):
        self._workerManager.deleteRunningWorker(name)

    def executeFrame(self, frame):
        cmd = frame["cmd"]

        self.startWorker(cmd)

class Executor(BaseExecutor):
    _instance = None

    @classmethod
    def excutor(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = cls(*args, **kwargs)

        return cls._instance