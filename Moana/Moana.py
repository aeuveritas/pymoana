import Moana.Core.Core as mCore
import Moana.Modules.Modules as mModules

class Moana:
    """Moana main class"""

    def __init__(self, params):
        """
        Moana class constructor
        :param params: I. parameters for config
        :return:
        """
        self._core = mCore.Core(params)
        self._modules = mModules.Modules()

    def _createWorkers(self):
        """
        create workers
            - Core
            - Module
        :return:
        """
        self._core.createWorkers()
        self._modules.createWorks()

    def run(self):
        """
        Start Moana main loop
            - Handle main queue
            - Create, run  workers
            - Initial ping
            - Control termination
        :return:
        """
        self._createWorkers()
        self._core.runWorkers()

        self._core.printDebugInfo()
        self._core.doInitialPing()

        ret = self._core.controlMainQueue()

        if ret == "normal":
            print("\nTerminated intentionally")
        elif ret == "keyboard":
            print("\nTerminated by keyboard interrupt")
            self._core.stop()
        else:
            print("\nTerminated by unknown interrupt")
            self._core.terminate()

        print("Bye~")

