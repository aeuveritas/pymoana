import Moana.Library.BaseClass.BassClass as mBaseClass

class ProcessClass(mBaseClass.BaseClass):
    def __init__(self, params = None):
        super(ProcessClass, self).__init__(params)

    def run(self, managerName):
        self._logger.critical("ProcessClass should be inherited")
        self._terminator.terminate()