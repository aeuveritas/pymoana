import Moana.Library.BaseClass.BassClass as mBaseClass
import Moana.Core.Messenger.Messenger as mMessenger
import Moana.Core.DataManager.DataManager as mDataManager
import Moana.Core.Executor.Executor as mExecutor
#import Moana.Core.Updater.Updater as mUpdater

class MainClass(mBaseClass.BaseClass):
    def __init__(self, params = None):
        super(MainClass, self).__init__(params)
        self._messenger = mMessenger.Messenger.messenger()
        self._dataManager = mDataManager.DataManager.dataManager()
        self._executor = mExecutor.Executor.excutor()
        #self._updater = mUpdater.Updater.updater()