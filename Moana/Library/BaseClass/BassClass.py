import Moana.Core.Logger.Logger as mLogger
import Moana.Core.QueueManager.QueueManager as mQueueManager
import Moana.Core.Ruler.Ruler as mRuler
import Moana.Core.Terminator.Terminator as mTerminator
import Moana.Core.WorkerManager.WorkerManager as mWorkerManager
import Moana.Core.Json.Json as mJson


class BaseClass:
    def __init__(self, params = None):
        self._ruler = mRuler.Ruler.ruler(params)
        self._logger = mLogger.Logger.logger()
        self._json = mJson.Json.json()
        self._workerManager = mWorkerManager.WorkerManager.workerManager()
        self._queueManager = mQueueManager.QueueManager.queueManager()
        self._terminator = mTerminator.Terminator.terminator()

        self._logger.info("Generated {0}".format(self))

    def getWorkerType(self):
        return type(self).__base__.__name__

    def __repr__(self):
        return type(self).__name__