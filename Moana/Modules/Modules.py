import Moana.Library.MainClass.MainClass as mMainClass
import Moana.Modules.Ping.Ping as mPing

class Modules(mMainClass.MainClass):
    def __init__(self):
        super(Modules, self).__init__()

        self._modulesList = self._ruler.getConfig("modules")

        self._modules = {}

        self.registerModules()

    def registerModules(self):
        for module in self._modulesList:
            if module == "ping":
                ping = mPing.Ping()
                self._modules[module] = ping
            else:
                self._logger.warning( \
                    "Not implemented module: {0}".format(module))

    def createWorks(self):
        for name, module in self._modules.items():
            module.createWorker()

    def getModule(self, module):
        return self._modules[module]