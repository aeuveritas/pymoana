from multiprocessing import Process

import Moana.Library.MainClass.MainClass as mMainClass
import Moana.Modules.Ping.PingReplyer as mPingReplyer
import Moana.Modules.Ping.PingRequester as mPingRequester

class Ping(mMainClass.MainClass):
    def __init__(self):
        super(Ping, self).__init__()
        self._class = self._ruler.getConfig("class")

        self._requesterQueue = self._queueManager.createQueue("PingRequester")
        self._replyerQueue = self._queueManager.createQueue("PingReplyer")

        self._pingRequester = None
        self._pintReplyer = None
        self._worker = None

    def createWorker(self):
        self._logger.debug("Create Ping Workers")

        if self._class == "master":
            self._createRequester()
        elif self._class == "servant":
            self._createReplyer()
        else:
            self._logger.critical("Wrong class type")
            self._terminator.stop()

    def _createRequester(self):
        self._pingRequester = mPingRequester.PingRequester()
        target = self._pingRequester.run
        self._executor.registerExecutor( \
            target, "PingRequester", self)

    def _createReplyer(self):
        self._pingReplyer = mPingReplyer.PingReplyer()
        target = self._pingReplyer.run
        self._worker = \
            Process(target=target,\
                    args=("PingReplyer",))
        self._workerManager.addWorker("PingReplyer", \
                                   self._worker, \
                                   self)

    def _toRequester(self, frame):
        self._requesterQueue.put(frame)

    def _toReplyer(self, frame):
        self._replyerQueue.put(frame)

    def putQueue(self, frame, caller):
        dest = frame["dest"]["role"]
        self._logger.debug("[F] {0} >> {1}: {2}".format(caller, dest, frame))

        if dest == "PingReplyer":
            self._toReplyer(frame)
        elif dest == "PingRequester":
            self._toRequester(frame)