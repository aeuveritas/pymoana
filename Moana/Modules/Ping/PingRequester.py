import Moana.Library.ProcessClass.ProcessClass as mProcessClass

"""
Process Name: PingRequester
Input Queue: requester
Manager: Ping
Input Method: toRequester
"""
class PingRequester(mProcessClass.ProcessClass):
    def __init__(self):
        super(PingRequester, self).__init__()
        self._machine = self._ruler.getConfig("machine")
        self._servants= {}

        self._getQueue = self._queueManager.loadQueue("PingRequester")

    def timeWait(self, waitTime = 1):
        frame = None
        servantsList = []

        while True:
            try:
                frame = self._getQueue.get(timeout = waitTime)
            except Exception:
                break

            servant = frame["msg"]
            servantsList.append(servant)

        return servantsList

    def updateServants(self, frame, servantsList):
        for servant in servantsList:
            self._servants[servant] = "ON"

        for k, v in self._servants.items():
            if k not in servantsList:
                self._servants[k] = "OFF"

        frame["msg"] = self._servants

    def run(self, managerName):
        self._workerManager.setCurrentWorker(managerName)

        # Send PingRequest
        src = self._json.getPacketSrc(self)

        machine = "ALL"
        exe = "Moana"
        role = "PingReplyer"

        dest = self._json.getDest(\
            machine=machine,\
            exe=exe, \
            role=role)

        packet = self._json.getPacket( \
            topic = "ALL", \
            src = src, \
            dest = dest, \
            cmd = "ping")

        messenger = self._workerManager.\
            getHandler("Publisher")
        messenger.sendPacket(packet, self)

        # Receive PingReplyer
        servantsList = self.timeWait()

        frame = self._json.getFrame( \
                src = "{0}".format(self), \
                cmd = "write")

        if len(servantsList) != 0:
            self.updateServants(frame, servantsList)

        self._logger.debug("Live servants: {0}".format(servantsList))

        dataManager = self._workerManager.\
            getHandler("DataManager")
        frame = dataManager.request(frame, self)

        self._json.movingFrame("DataManager", self, frame)

        if frame["msg"] != "updated":
            self._logger.critical(
                "Database update failed: {0}".format(frame))

        machine = self._ruler.getConfig("machine")
        exe = "Moana"
        role = "Core"
        dest = self._json.getDest( \
            machine=machine, \
            exe=exe, \
            role = role)

        frame = self._json.getFrame( \
            src = "{0}".format(self), \
            dest = dest, \
            cmd = "finish")

        core = self._workerManager.\
            getHandler("Core")
        core.putQueue(frame, self)

