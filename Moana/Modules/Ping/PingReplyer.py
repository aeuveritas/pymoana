import Moana.Library.ProcessClass.ProcessClass as mProcessClass

"""
Process Name: PingReplyer
Input Queue: replyer
Manager: Ping
Input Method: toReplyer
"""
class PingReplyer(mProcessClass.ProcessClass):
    def __init__(self):
        super(PingReplyer, self).__init__()
        self._machine = self._ruler.getConfig("machine")
        self._getQueue = self._queueManager.loadQueue("PingReplyer")

    def run(self, managerName):
        self._workerManager.setCurrentWorker(managerName)

        while True:
            frame = self._getQueue.get()

            cmd = frame["cmd"]

            if cmd == "ping":
                topic = self._ruler.getConfig("master")

                src = self._json.getPacketSrc(self)

                dest = self._json.getDest(\
                    machine = self._ruler.getConfig("master"),\
                    exe = "Moana", \
                    role = "PingRequester")

                packet = self._json.getPacket( \
                    topic=topic, \
                    src=src, \
                    dest=dest, \
                    cmd="ping", \
                    msg="{0}".format(self._machine))

                messenger = self._workerManager.\
                    getHandler("Publisher")
                messenger.sendPacket(packet, self)

            else:
                self._logger.debug("Wrong command: {0}".format(cmd))
