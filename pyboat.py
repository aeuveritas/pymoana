#!/usr/bin/env python3
import ArgParser as mArgParser
import Boat.Boat as mBoat

def main():
    parser = mArgParser.ArgParser()
    params = parser.parseBoat()

    boat = mBoat.Boat(params)

    boat.run()

if __name__ == "__main__":
    main()
