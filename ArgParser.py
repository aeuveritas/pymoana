import sys
import argparse

class ArgParser:
    def __init__(self):
        pass

    def parseMoana(self):
        ap = argparse.ArgumentParser()
        sp = ap.add_subparsers()

        subParams = {}
        subParams["run"] = sp.add_parser("run", help="run")
        subParams["debug"] = sp.add_parser("debug", help="debug")

        for k, v in subParams.items():
            sub = subParams[k]

            sub.set_defaults(exe="Moana")
            sub.set_defaults(action=k)

            sub.add_argument(\
                "-c", "--configFile", \
                help="config file", \
                default="./config")

            if k == "debug":
                sub.add_argument(\
                    "-cl", "--class", help="class", default=None)
                sub.add_argument(\
                    "-ma", "--machine", help="machine", default=None)
                sub.add_argument(\
                    "-me", "--messenger", \
                    help="publisher, subscriber or both", \
                    default=None)

        params = vars(ap.parse_args())

        if not params:
            ap.print_help()
            sys.exit(2)

        return params

    def parseBoat(self):
        ap = argparse.ArgumentParser()
        sp = ap.add_subparsers()

        subParams = {}
        subParams["run"] = sp.add_parser("run", \
                                         help="run")
        subParams["debug"] = sp.add_parser("debug", \
                                           help="debug")

        for k, v in subParams.items():
            sub = subParams[k]

            sub.set_defaults(exe="Boat")
            sub.set_defaults(action=k)

            sub.add_argument("-c", "--configFile", \
                             help="config file", \
                             default="./Boat/config")

        params = vars(ap.parse_args())

        if not params:
            ap.print_help()
            sys.exit(2)

        return params